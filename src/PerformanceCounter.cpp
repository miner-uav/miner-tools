#include "miner_tools/PerformanceCounter.h"
#include <boost/thread/locks.hpp>

namespace miner_tools
{

PerformanceCounter::PerformanceCounter(uint32_t window_count)
    : mWindowCount(window_count)

{
    mTimestamps = new double[window_count + 1];
    reset();
}

PerformanceCounter::~PerformanceCounter()
{
    delete mTimestamps;
}

void PerformanceCounter::tick()
{
    tick(ros::Time::now());
}

void PerformanceCounter::tick(const ros::Time &time)
{
    tick(time.toSec());
}

void PerformanceCounter::tick(double time)
{
    boost::mutex::scoped_lock lock(mTimestampsMutex);

    if (mTicksCount == 0)
    {
        mFirstTs = time;
    }
    mTimestamps[mCurrentIndex] = time;

    mCurrentIndex = ++mCurrentIndex % (mWindowCount + 1);

    ++mTicksCount;
}

void PerformanceCounter::reset()
{
    boost::mutex::scoped_lock lock(mTimestampsMutex);

    memset(mTimestamps, 0xFA, sizeof(double)*(mWindowCount+1));
    mCurrentIndex = 0;
    mTicksCount   = 0;
}

uint32_t PerformanceCounter::getEventCount()
{
    return mTicksCount;
}

double PerformanceCounter::getFrequency()
{
    boost::mutex::scoped_lock lock(mTimestampsMutex);

    // we need at least 2 ticks to calculate frequency
    if (mTicksCount < 2)
    {
        return 0.0;
    }

    double duration;
    uint32_t period_count;
    // we did not rolled over mCurrentIndex yet
    if (mTicksCount == mCurrentIndex)
    {
        duration     = mTimestamps[mCurrentIndex - 1] - mTimestamps[0];
        period_count = mCurrentIndex - 1;
        double freq = period_count / duration;
        return freq;
    }
    else
    {
        uint32_t start_index = mCurrentIndex;
        uint32_t end_index   = (mCurrentIndex + mWindowCount) % (mWindowCount + 1);
        period_count         = mWindowCount;
        duration             = mTimestamps[end_index] - mTimestamps[start_index];
        double freq = period_count / duration;
        return freq;
    }
}

} // namespace miner_tools
