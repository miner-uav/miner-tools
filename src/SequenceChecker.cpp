#include "miner_tools/SequenceChecker.h"

namespace miner_tools
{

SequenceChecker::SequenceChecker()
    : mLastSeq(0)

{
    reset();
}

bool SequenceChecker::sequence(uint32_t seq)
{
    boost::mutex::scoped_lock lock(mTimestampsMutex);
    if (mIsInitialised)
    {
        int diff = seq - mLastSeq;
        //if(diff == 1)
    }
    else
    {
        mLastSeq = seq;
        mIsInitialised = true;
    }
}

void SequenceChecker::reset()
{
    boost::mutex::scoped_lock lock(mTimestampsMutex);

    mLastSeq       = 0;
    mIsInitialised = false;
    mMissingSeq.clear();
}

} // namespace miner_tools
