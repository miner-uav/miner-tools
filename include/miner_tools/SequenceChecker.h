#ifndef SEQUENCECHECKER_H
#define SEQUENCECHECKER_H

#include <vector>
#include <stdint.h>

#include <boost/thread/pthread/mutex.hpp>

namespace miner_tools {

using namespace std;

class SequenceChecker
{
public:
    SequenceChecker();
    bool sequence(uint32_t seq);
    void reset();


private:
    bool mIsInitialised;
    boost::mutex mTimestampsMutex;
    vector<uint32_t> mMissingSeq;
    uint32_t mLastSeq;
};

} // namespace miner_tools

#endif // SEQUENCECHECKER_H
