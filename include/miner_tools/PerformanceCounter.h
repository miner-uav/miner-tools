#ifndef PERFORMANCECOUNTER_H
#define PERFORMANCECOUNTER_H

#include <boost/thread/pthread/mutex.hpp>
#include <ros/ros.h>
#include <vector>

namespace miner_tools
{

using namespace std;

class PerformanceCounter
{
  public:
    PerformanceCounter(uint32_t window_count = 100);
    ~PerformanceCounter();
    void tick();
    void tick(const ros::Time &time);
    void tick(double time);
    void reset();
    uint32_t getEventCount();
    double getFrequency();

  private:
    boost::mutex mTimestampsMutex;

    int mWindowCount;
    uint32_t mTicksCount;
    double *mTimestamps;
    uint32_t mCurrentIndex;
    double mFirstTs;
};

} // namespace miner_tools

#endif // PERFORMANCECOUNTER_H
