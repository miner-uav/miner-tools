// Bring in my package's API, which is what I'm testing
#include "miner_tools/PerformanceCounter.h"
// Bring in gtest
#include <gtest/gtest.h>
// Declare a test

using namespace miner_tools;

TEST(PerformanceCounter, constructor)
{
    PerformanceCounter pc;
    EXPECT_DOUBLE_EQ(0.0, pc.getFrequency());
}

// Declare another test
TEST(PerformanceCounter, notEnoughTicks)
{
    PerformanceCounter pc;
    pc.tick();
    EXPECT_DOUBLE_EQ(0.0, pc.getFrequency());
}
// Declare another test
TEST(PerformanceCounter, justEnoughTicks)
{
    PerformanceCounter pc;
    pc.tick();
    pc.tick();
    EXPECT_TRUE(pc.getFrequency() > 200.0);
}
TEST(PerformanceCounter, thousandTicks)
{
    PerformanceCounter pc;

    for (int i = 0; i < 1000; ++i)
    {
        pc.tick();
    }
    EXPECT_TRUE(pc.getFrequency() > 200.0);
    EXPECT_DOUBLE_EQ(pc.getEventCount(), 1000);
}


/**
 * @brief TEST
 */
TEST(PerformanceCounter, freqCalcWindowSizeInvariant)
{

    double time   = 0;
    double period = 0.1;
    double freq   = 1 / period;

    for (int testId = 10; testId < 103; ++testId)
    {
        PerformanceCounter pc(testId);
        for (int i = 0; i < 1000; ++i)
        {
            pc.tick(time);
            time += period;
            if (i >= 2)
            {
                double actualFreq = pc.getFrequency();
                double error = fabs(freq - actualFreq);
                EXPECT_TRUE(error < 1.0e-9);
            }
        }
    }
}

TEST(PerformanceCounter, resetTicks)
{
    PerformanceCounter pc;

    for (int i = 0; i < 1000; ++i)
    {
        pc.tick();
    }
    EXPECT_TRUE(pc.getFrequency() > 200.0);
    EXPECT_DOUBLE_EQ(1000, pc.getEventCount());
    pc.reset();
    EXPECT_EQ(0, pc.getEventCount());
}

// Run all the tests that were declared with TEST()
int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "tester");
    ros::NodeHandle nh;
    return RUN_ALL_TESTS();
}
